#!/usr/bin/python3

import os
import sys
import subprocess
import json


from techworks.drivers.ShellCommandDriver import ShellCommandDriver


# ============================================
# Driver which implements python-ansible API
# ============================================
class TerraformDriver() :

    def printPretty(self, json_obj):
        parsed = json.loads(json_obj)
        print (json.dumps(parsed, indent=4, sort_keys=True, ensure_ascii=False))

    def printPrettyObj(self, python_obj):
        print (json.dumps(python_obj, indent=4, sort_keys=True, ensure_ascii=False))

    def plan(self):
        print ("Terraform : Plan")
        os.chdir("../terraform")
        print (os.getcwd())
        linux = ShellCommandDriver()
        output = linux.runShellCommand(['bash', './terraform-plan.sh'])
        print (output)
        return output

    def apply(self):
        print ("Terraform : Apply")
        os.chdir("../terraform")
        print (os.getcwd())
        linux = ShellCommandDriver()
        output = linux.runShellCommand(['bash', './terraform-apply.sh'])
        print (output)
        return output

    def destroy(self):
        print ("Terraform : Destroy")
        os.chdir("../terraform")
        print (os.getcwd())
        linux = ShellCommandDriver()
        output = linux.runShellCommand(['bash', './terraform-destroy.sh'])
        print (output)
        return output

    def show(self):
        print ("\n\nTerraform : Show")
        os.chdir("../terraform")
        print (os.getcwd())
        linux = ShellCommandDriver()
        output = linux.runShellCommand(['bash', './terraform-show.sh'])
        print (output)
        return output

    def output(self):
        print ("\n\nTerraform : Output")
        os.chdir("../terraform")
        print (os.getcwd())
        linux = ShellCommandDriver()
        output = linux.runShellCommand(['bash', './terraform-output.sh'])
        print (output)

        jsonStart = output.find("{")
        jsonEnd = output.find("}")
        result = output[jsonStart:jsonEnd+1]
        print(result)
        return result

