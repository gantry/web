#!/usr/bin/python3
import lxc
import sys



# ============================================
# Service which proxies calls to Alfresco API
# ============================================
class LxdDriver() :

    def run(self, name):
        print ("Running LXD")

        for container in lxc.list_containers(as_object=True):
            print("Container")
            print(container.state)
            # Start the container (if not started)
            started = False
            if not container.running:
                if not container.start():
                    continue
                started = True

            if not container.state == "RUNNING":
                continue

            # Wait for connectivity
            if not container.get_ips(timeout=30):
                continue

            # Run the updates
            container.attach_wait(lxc.attach_run_command,
                                  ["apt-get", "update"])
            container.attach_wait(lxc.attach_run_command,
                                  ["apt-get", "dist-upgrade", "-y"])

            # Shutdown the container
            if started:
                if not container.shutdown(30):
                    container.stop()

        # //////////////////////////


        # Setup the container object
        c = lxc.Container("gitlab")
        if c.defined:
            print("Container already exists", file=sys.stderr)
            sys.exit(1)

        config = {"dist": "ubuntu", "release": "xenial", "arch": "amd64"}

        # Create the container rootfs
        if not c.create("download", lxc.LXC_CREATE_QUIET, config):
            print("Failed to create the container rootfs", file=sys.stderr)
            sys.exit(1)

        # Start the container
        if not c.start():
            print("Failed to start the container", file=sys.stderr)
            sys.exit(1)

        # Query some information
        print("Container state: %s" % c.state)
        print("Container PID: %s" % c.init_pid)

        # Stop the container
        if not c.shutdown(30):
            print("Failed to cleanly shutdown the container, forcing.")
            if not c.stop():
                print("Failed to kill the container", file=sys.stderr)
                sys.exit(1)

        # Destroy the container
        if not c.destroy():
            print("Failed to destroy the container.", file=sys.stderr)
            sys.exit(1)