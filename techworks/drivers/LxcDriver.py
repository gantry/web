# LXC Python Library
# for compatibility with LXC 0.8 and 0.9
# on Ubuntu 12.04/12.10/13.04

# Author: TechWorks
# Contact:

# The MIT License (MIT)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# This permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import subprocess
import os


def _run(cmd):
    '''
    To run command easier
    '''
    return subprocess.check_output('{}'.format(cmd), shell=True, universal_newlines=True)


class ContainerAlreadyExists(Exception):
    pass


class ContainerDoesntExists(Exception):
    pass


class ContainerAlreadyRunning(Exception):
    pass


class ContainerNotRunning(Exception):
    pass


def exists(container):
    '''
    Check if container exists
    '''
    # print ("Container Exists : " + container)
    # print (container in ls())
    return (container in ls())

def create(container, template='ubuntu', storage=None, xargs=None):
    '''
    Create a container (without all options)
    Default template: Ubuntu
    '''

    if exists(container):
        raise ContainerAlreadyExists(
            'Container {} already created!'.format(container))

    # lxc launch ubuntu:16.04 $CONTAINER_NAME
    command = 'lxc launch ubuntu:16.04 {}'.format(container)
    # command += ' -t {}'.format(template)

    if storage:
        command += ' -B {}'.format(storage)

    if xargs:
        command += ' -- {}'.format(xargs)

    return _run(command)

def initialize(container):
    '''
    Initialize Keys, Mounts, etc
    '''

    # What directory are we in??
    # cwd = os.getcwd()
    # print(cwd)
    os.chdir("../ansible")
    # Initialize Keys, Mounts, etc
    command = './init-container.sh {}'.format(container)

    return _run(command)


def clone(orig=None, new=None, snapshot=False):
    '''
    Clone a container (without all options)
    '''

    # ==========================================
    # TODO : Refactor this section for snapshots
    # Cloning doesn't seem to exist anymore
    # Maybe they want us to snapshot/image then launch new vms?
    # Not sure cloning is a requirement anyway..
    # ==========================================

    if orig and new:
        if exists(new):
            raise ContainerAlreadyExists(
                'Container {} already exist!'.format(new))

        command = 'lxc clone -o {} -n {}'.format(orig, new)
        if snapshot:
            command += ' -s'

        return _run(command)


def info(container):
    '''
    Check info from lxc-info
    '''

    # print ("Container Info: " + container)
    if not exists(container):
        raise ContainerDoesntExists(
            'Container {} does not exist!'.format(container))


    # TODO : Get the entire lxc info object & parse more data from it..
    # The "lxc info" command is revamped in LXC2.0
    # Contains network info etc
    # lxc info test3 --show-log

    # EG =====================================================
    # root@gantry:/home/ubuntu# lxc info test3 --show-log
    # Name: test3
    # Remote: unix:/var/lib/lxd/unix.socket
    # Architecture: x86_64
    # Created: 2017/11/30 01:34 UTC
    # Status: Running
    # Type: persistent
    # Profiles: default
    # Pid: 14721
    # Ips:
    # eth0:	inet	10.161.167.251	veth6H7LLG
    # eth0:	inet6	fd42:b96:2965:d5be:216:3eff:fea1:69ad	veth6H7LLG
    # eth0:	inet6	fe80::216:3eff:fea1:69ad	veth6H7LLG
    # lo:	inet	127.0.0.1
    # lo:	inet6	::1
    # Resources:
    # Processes: 27
    # CPU usage:
    # CPU usage (in seconds): 5
    # Memory usage:
    # Memory (current): 41.39MB
    # Memory (peak): 164.77MB
    # Network usage:
    # eth0:
    # Bytes received: 24.93kB
    # Bytes sent: 5.60kB
    # Packets received: 204
    # Packets sent: 47
    # lo:
    # Bytes received: 0B
    # Bytes sent: 0B
    # Packets received: 0
    # Packets sent: 0
    #
    # Log:
    #
    # EG =====================================================
    # print ("Printing Output")
    output = _run('lxc info {} --show-log |grep -i "Status\|Name"'.format(container)).splitlines()

    # print ("Got Output")
    # print (output)

    state = output[1].split()[1]
    # print ("State : " + state)

    # if state == 'STOPPED':
    #     pid = "0"
    # else:
    pid = output[0].split()[1]
    # print ("Name : " + pid)

    # print ("Leaving Info")
    return {'state': state,
            'pid': pid}

def ip_address(container, assume_running=False):

    # print ("Container")
    # print (container)
    # print ("assume_running")
    # print (assume_running)
    # print ("info")
    # print (info(container))
    # print ("end")

    try:
        if assume_running or (info(container)['Status'] == 'Running'):

            print ("=============Getting IpV4 Address=============")
            # lxc info test0 | grep inet | awk '{print $3}'
            result = _run('lxc info %s | grep inet | awk \'{print $3}\' ' % container).split('\n')

            # print ("Result")
            print (result)

            # print ("Returning")
            print (result[0])
            print ("==============================================")
            return result[0]

            # return _run('lxc info -n %s -iH' % container)
    except:
        pass
    return ''


def ls():
    '''
    List containers directory

    Note: Directory mode for Ubuntu 12/13 compatibility
    '''

    base_path = '/var/lib/lxd/containers'

    try:
        ct_list = [x for x in os.listdir(base_path)
                   if os.path.isdir(os.path.join(base_path, x))]
    except OSError:
        print ("Cant find directory..")
        print ("Are you sure you're running as root?!")
        ct_list = []

    # print (sorted(ct_list))
    return sorted(ct_list)


def ls_new():
    '''
    List containers directory

    Note: Directory mode for Ubuntu 12/13 compatibility
    '''

    ct_list = [_run('lxc list -cn | grep -v NAME | grep -v "+" | cut -d "|" -f 2 | cut -d " " -f 2  | tr -d "\n"')]
    # ct_list = [_run('lxc list -cn | grep -v NAME | grep -v "+" | cut -d "|" -f 2 | tr -d "\n"')]
    # ct_list = [_run('lxc list -cn | grep -v NAME | grep -v "+" | cut -d "|" -f 2 | cut -d " " -f 2 ')]
    str_list = filter(None, ct_list) # fastest
    print ("LXC List ========== ")
    print (ct_list)
    print (str_list)
    print ("END LXC List ========== !! ")
    return ct_list



def listx():
    '''
    List all containers with status (Running, Frozen or Stopped) in a dict
    Same as lxc-list or lxc-ls --fancy (0.9)
    '''

    stopped = []
    frozen = []
    running = []

    for container in ls():
        print ("Container: " + container)

        state = info(container)['state']
        print ("State: " + state)

        if state == 'Running':
            # print ("Add to Running: ")
            running.append(container)
        elif state == 'Frozen':
            # print ("Add to Frozen: ")
            frozen.append(container)
        elif state == 'Stopped':
            # print ("Add to Stopped: ")
            stopped.append(container)

    return {'RUNNING': running,
            'FROZEN': frozen,
            'STOPPED': stopped}


def running():
    return listx()['RUNNING']


def frozen():
    return listx()['FROZEN']


def stopped():
    return listx()['STOPPED']


def start(container):
    '''
    Starts a container
    '''

    if not exists(container):
        raise ContainerDoesntExists(
            'Container {} does not exists!'.format(container))

    if container in running():
        raise ContainerAlreadyRunning(
            'Container {} is already running!'.format(container))

    return _run('lxc start {}'.format(container))


def stop(container):
    '''
    Stops a container
    '''

    if not exists(container):
        raise ContainerDoesntExists(
            'Container {} does not exists!'.format(container))

    if container in stopped():
        raise ContainerNotRunning(
            'Container {} is not running!'.format(container))

    return _run('lxc stop {}'.format(container))


def freeze(container):
    '''
    Freezes a container
    '''

    if not exists(container):
        raise ContainerDoesntExists(
            'Container {} does not exists!'.format(container))

    if not container in running():
        raise ContainerNotRunning(
            'Container {} is not running!'.format(container))

    return _run('lxc pause {}'.format(container))


def unfreeze(container):
    '''
    Unfreezes a container
    '''

    if not exists(container):
        raise ContainerDoesntExists(
            'Container {} does not exists!'.format(container))

    if not container in frozen():
        raise ContainerNotRunning(
            'Container {} is not frozen!'.format(container))

    return _run('lxc start {}'.format(container))


def destroy(container):
    '''
    Destroys a container
    '''

    print ("Destroying : " + container)

    if not exists(container):
        raise ContainerDoesntExists(
            'Container {} does not exists!'.format(container))

    print ("Preparing to Delete : " + container)
    result = _run('lxc delete {} --force'.format(container))
    print (result)

    return result

# def checkconfig():
#     '''
#     Returns the output of lxc-checkconfig (colors cleared)
#     '''
#
#     out = _run('lxc-checkconfig')
#
#     if out:
#         return out.replace('[1;32m', '').replace('[1;33m', '') \
#             .replace('[0;39m', '').replace('[1;32m', '') \
#             .replace('\x1b', '').replace(': ', ':').split('\n')
#
#     return out


def cgroup(container, key, value):
    if not exists(container):
        raise ContainerDoesntExists(
            'Container {} does not exist!'.format(container))

    # return _run('lxc-cgroup -n {} {} {}'.format(container, key, value))
    return "{}"

