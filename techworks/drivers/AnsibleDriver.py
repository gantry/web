#!/usr/bin/python3

import os
import sys
import subprocess
import json
import glob
from collections import namedtuple
from tornado.concurrent import run_on_executor


from techworks.drivers.ShellCommandDriver import ShellCommandDriver
from techworks.services.websocket.AnsibleCallbackWebSocket import AnsibleCallbackWebSocket
# from techworks.human_log import CallbackModule

# from ansible.parsing.dataloader import DataLoader
# from ansible.vars.manager import VariableManager
# from ansible.inventory.manager import InventoryManager
# from ansible.executor.playbook_executor import PlaybookExecutor
# from ansible import utils
# from ansible import callbacks
# from ansible.callbacks import display
# from ansible.color import ANSIBLE_COLOR, stringc



# ============================================
# Driver which implements python-ansible API
# ============================================
class AnsibleDriver() :


    def printPretty(self, json_obj):
        parsed = json.loads(json_obj)
        print (json.dumps(parsed, indent=4, sort_keys=True, ensure_ascii=False))

    def printPrettyObj(self, python_obj):
        print (json.dumps(python_obj, indent=4, sort_keys=True, ensure_ascii=False))

    def getPlaybookList(self):
        '''
        Get Playbook List
        '''

        lastDir = os.getcwd()
        os.chdir("../ansible/playbooks")
        playbooks = []
        for file in glob.glob("*.yml"):
            print(file[:-4])
            playbooks.append(file[:-4])

        os.chdir(lastDir)
        playbooks.sort()
        return playbooks

        # Run Ansible Playbook via CLI
    def runPlaybookManual(self, name, playbook):

        print ("Running Playbook")
        lastDir = os.getcwd()
        os.chdir("../ansible")

        # self.hostsFile = "./.vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory"
        self.hostsFile = "./hosts"

        path = "./playbooks/" + playbook

        # Run Shell-Command ..
        # self.runShellCommand(['ls', '-l'])
        # self.runShellCommand(['ansible-playbook', '-vvvv', path, "-i", self.hostsFile, "-l", name])
        linux = LinuxCommand()
        linux.runShellCommandRealTime(['ansible-playbook', '-vvvv', path, "-i", self.hostsFile, "-l", name])

        # Simple to Run Shell Command ..
        # playbookCommand = "ansible-playbook -vvvv " + path + "-i " + self.hostsFile + " -l " + name
        # print(playbookCommand )
        # result = os.system(playbookCommand)

        print ("Playbook Complete !!")
        # print (result)
        print ("====================")
        os.chdir(lastDir)
        return;


    # Run Ansible Playbook via python-ansible API
    # def runPlaybookAPI(self, hostsFile, hostsList, playbookFile) :
    #
    #     print ("AnsibleDriver : Running Playbook")
    #     print (hostsFile)
    #     print (hostsList)
    #     print (playbookFile)
    #
    #     variable_manager = VariableManager()
    #     loader = DataLoader()
    #
    #
    #     print ("Variables Loaded")
    #
    #     inventory = InventoryManager(loader=loader, variable_manager=variable_manager,  host_list=hostsFile)
    #     print ("Inventory Loaded")
    #     print(inventory)
    #     inventory.subset(hostsList)
    #
    #     print ("Inventory Hosts ")
    #
    #     playbook_path = playbookFile
    #     if not os.path.exists(playbook_path):
    #         print ('[INFO] The playbook does not exist')
    #         sys.exit()
    #
    #     utils.VERBOSITY = 4
    #
    #     Options = namedtuple('Options', ['listtags', 'listtasks', 'listhosts', 'syntax', 'connection','module_path', 'forks', 'remote_user', 'private_key_file', 'ssh_common_args', 'ssh_extra_args', 'sftp_extra_args', 'scp_extra_args', 'become', 'become_method', 'become_user', 'verbosity', 'check'])
    #     options = Options(
    #         listtags=False,
    #         listtasks=False,
    #         listhosts=False,
    #         syntax=False,
    #         connection='ssh',
    #         module_path=None,
    #         forks=100,
    #         remote_user='vagrant',
    #         private_key_file=None,
    #         ssh_common_args=None,
    #         ssh_extra_args=None,
    #         sftp_extra_args=None,
    #         scp_extra_args=None,
    #         become=True,
    #         become_method="sudo",
    #         become_user='root',
    #         verbosity=utils.VERBOSITY,
    #         check=False)
    #
    #     print ("Options Loaded")
    #
    #     # variable_manager.extra_vars = {'hosts': 'db'} # This can accomodate various other command line arguments.`
    #     passwords = {}
    #
    #
    #     # Make sure we aggregate the stats
    #     # stats = callbacks.AggregateStats()
    #
    #     print ("Executing...")
    #
    #     pbex = PlaybookExecutor(
    #         playbooks=[playbook_path],
    #         inventory=inventory,
    #         variable_manager=variable_manager,
    #         loader=loader,
    #         options=options,
    #         passwords=passwords)
    #
    #     # cb2 = CallbackModule()
    #     cb = ResultAccumulator()
    #     pbex._tqm._stdout_callback = cb
    #
    #     # Run Playbook
    #     results = pbex.run()
    #
    #     print("FailedHosts")
    #     failed_hosts = pbex._tqm._failed_hosts
    #     print(failed_hosts)
    #
    #     print("UnreachableHosts")
    #     unreachable_hosts = pbex._tqm._unreachable_hosts
    #     print(failed_hosts)
    #
    #     # Test if success for record_logs
    #     run_success = True
    #     stats = pbex._tqm._stats
    #     hosts = sorted(stats.processed.keys())
    #     for h in hosts:
    #         t = stats.summarize(h)
    #         print("Stat Summary : ")
    #         self.printPrettyObj(t)
    #         if t['unreachable'] > 0 or t['failures'] > 0:
    #             run_success = False
    #
    #     if(run_success):
    #         print("Success !!!")
    #     else:
    #         print("Failed !!!")
    #
    #     print("Playbook Complete")
    #     print(results)
    #     return results




###########################################
# Alt Impls
###########################################

# import json
# from collections import namedtuple
# from ansible.parsing.dataloader import DataLoader
# from ansible.vars import VariableManager
# from ansible.inventory import Inventory
# from ansible.playbook.play import Play
# from ansible.executor.task_queue_manager import TaskQueueManager
# from ansible.plugins.callback import CallbackBase
#

# class ResultCallback(CallbackBase):
#     """A sample callback plugin used for performing an action as results come in
#
#     If you want to collect all results into a single object for processing at
#     the end of the execution, look into utilizing the ``json`` callback plugin
#     or writing your own custom callback plugin
#     """
#     def v2_runner_on_ok(self, result, **kwargs):
#         """Print a json representation of the result
#
#         This method could store the result in an instance attribute for retrieval later
#         """
#         host = result._host
#         print json.dumps({host.name: result._result}, indent=4)
#
# Options = namedtuple('Options', ['connection', 'module_path', 'forks', 'become', 'become_method', 'become_user', 'check'])
#
# # initialize needed objects
# variable_manager = VariableManager()
# loader = DataLoader()
# options = Options(connection='local', module_path='/path/to/mymodules', forks=100, become=None, become_method=None, become_user=None, check=False)
# passwords = dict(vault_pass='secret')
#
# # Instantiate our ResultCallback for handling results as they come in
# results_callback = ResultCallback()
#
# # create inventory and pass to var manager
# inventory = Inventory(loader=loader, variable_manager=variable_manager, host_list='localhost')
# variable_manager.set_inventory(inventory)
#
# # create play with tasks
# play_source =  dict(
#         name = "Ansible Play",
#         hosts = 'localhost',
#         gather_facts = 'no',
#         tasks = [
#             dict(action=dict(module='shell', args='ls'), register='shell_out'),
#             dict(action=dict(module='debug', args=dict(msg='{{shell_out.stdout}}')))
#          ]
#     )
# play = Play().load(play_source, variable_manager=variable_manager, loader=loader)
#
# # actually run it
# tqm = None
# try:
#     tqm = TaskQueueManager(
#               inventory=inventory,
#               variable_manager=variable_manager,
#               loader=loader,
#               options=options,
#               passwords=passwords,
#               stdout_callback=results_callback,  # Use our custom callback instead of the ``default`` callback plugin
#           )
#     result = tqm.run(play)
# finally:
#     if tqm is not None:
#         tqm.cleanup()
#




# # Ansible Playbook API
# import ansible.runner
# import ansible.playbook
# import ansible.inventory
# from ansible import callbacks
# from ansible import utils
# import json
#
#
# class AnsibleDriver() :
#
#
#     def runPlaybook(self):
#
#         runner = ansible.runner.Runner(
#             module_path="",
#             module_name='install-common',
#             module_args='',
#             pattern='web',
#         )
#         facts = runner.run()
#         print (facts)
#         return facts
#
#     # # Run Ansible Playbook (Python API)
#     # def runPlaybook(self, floatingIP, groupName):
#     #
#     #     # container = lxc.Container("my_container2")
#     #     # container.create("ubuntu")
#     #     # container.start()
#     #     # print(container.get_ips(timeout=10))
#     #     # floatingIP=container.get_ips(timeout=10)
#     #     # #container.shutdown(timeout=10)
#     #     # #container.destroy()
#     #
#     #     ## first of all, set up a host (or more)
#     #     # name = '10.11.12.66',
#     #     example_host = ansible.inventory.host.Host(
#     #         name = floatingIP,
#     #         port = 22
#     #     )
#     #     # with its variables to modify the playbook
#     #     example_host.set_variable( 'var', 'foo')
#     #
#     #     ## secondly set up the group where the host(s) has to be added
#     #     example_group = ansible.inventory.group.Group(
#     #         name = 'sample_group_name'
#     #     )
#     #     example_group.add_host(example_host)
#     #
#     #     ## the last step is set up the invetory itself
#     #     example_inventory = ansible.inventory.Inventory()
#     #     example_inventory.add_group(example_group)
#     #     example_inventory.subset('sample_group_name')
#     #
#     #     # setting callbacks
#     #     stats = callbacks.AggregateStats()
#     #     playbook_cb = callbacks.PlaybookCallbacks(verbose=utils.VERBOSITY)
#     #     runner_cb = callbacks.PlaybookRunnerCallbacks(stats, verbose=utils.VERBOSITY)
#     #
#     #     # creating the playbook instance to run, based on "install-common.yml" file
#     #     pb = ansible.playbook.PlayBook(
#     #         playbook = "install-common.yml",
#     #         stats = stats,
#     #         callbacks = playbook_cb,
#     #         runner_callbacks = runner_cb,
#     #         inventory = example_inventory,
#     #         check=True
#     #     )
#     #
#     #     # running the playbook
#     #     pr = pb.run()
#     #
#     #     # print the summary of results for each host
#     #     print(json.dumps(pr, sort_keys=True, indent=4, separators=(',', ': ')))
#     #
#     #     return;
