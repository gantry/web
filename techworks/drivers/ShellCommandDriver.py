#!/usr/bin/python3

import os
import sys
import subprocess
import json


from techworks.services.websocket.WebSocketConnection import WebSocketConnection


# ============================================
# Driver which implements python-ansible API
# ============================================
class ShellCommandDriver() :

    def printPretty(self, json_obj):
        parsed = json.loads(json_obj)
        print (json.dumps(parsed, indent=4, sort_keys=True, ensure_ascii=False))

    def printPrettyObj(self, python_obj):
        print (json.dumps(python_obj, indent=4, sort_keys=True, ensure_ascii=False))

    def run(self, cmd):
        return subprocess.check_output('{}'.format(cmd), shell=True, universal_newlines=True)

    def runShellCommand(self, cmd):
        # Setup command and direct the output to a pipe
        print ("Running Shell Command")
        p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)

        # Run the command
        output = p1.communicate()[0]
        # print ("Command Output")
        # print (output)

        for connection in WebSocketConnection.participants:
            connection.sendMessage(output)

        return output

    # USE THIS VERSION FOR REAL-TIME FEEDBACK
    def runShellCommandRealTime(self, cmd):
        # process = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        while True:
            output = process.stdout.readline().decode()
            if output == '' and process.poll() is not None:
                break
            if output:
                print (output.strip())
                for connection in WebSocketConnection.participants:
                    connection.sendMessage(output.strip())

        rc = process.poll()
        return rc

    # Remote SSH Tail Virtual Machine Logs
    # def monitorInstance(self, ipAddress):
    #
    #     print("Monitoring Instance")
    #
    #     # man ssh
    #     # -n   Redirects stdin from /dev/null (actually, prevents reading from stdin).  This must be used when ssh
    #     #      is run in the background.  A common trick is to use this to run X11 programs on a remote machine.
    #     #      For example, ssh -n shadows.cs.hut.fi emacs & will start an emacs on shadows.cs.hut.fi, and the X11
    #     #      connection will be automatically forwarded over an encrypted channel.  The ssh program will be put
    #     #      in the background.  (This does not work if ssh needs to ask for a password or passphrase; see also
    #     #      the -f option.)
    #     cmd = 'ssh -n -i keys/engility.pem vagrant@' + ipAddress + ' "sudo tail -f /opt/outputfile.txt" '
    #
    #     """from http://blog.kagesenshi.org/2008/02/teeing-python-subprocesspopen-output.html
    #     """
    #
    #     print ("Running Shell Command")
    #     p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #     stdout = []
    #     while True:
    #         line = p.stdout.readline()
    #         if not line :
    #             stdout.append(line)
    #             print (line)
    #             for connection in WebSocketConnection.participants:
    #                 connection.sendMessage(line)
    #
    #         if line == '' and p.poll() != None:
    #             break
    #
    #     print("Monitoring Finished")
    #     return ''.join(stdout)

