#!/usr/bin/python3

import os
# from nginxparser import load
# from nginxparser import dumps
# from nginx.config.builder import NginxConfigBuilder

import json


class NginxDriver() :

    jsonConfigFile = './containers.json'
    nginxConfigPath = '/etc/nginx/sites-available/microservices/'


    ##########################################3
    # Get/Save JSON Config block
    ##########################################3


    # Example Data Access
    # secret_key = config['DEFAULT']['SECRET_KEY']  # 'secret-key-of-myapp'
    # ci_hook_url = config['CI']['HOOK_URL']  # 'web-hooking-url-from-ci-service'
    def getJsonConfig(self):

        config = ""
        print("Loading File")
        with open(self.jsonConfigFile, 'r') as f:
            config = json.load(f)

        print(config)
        return config

    def addJsonConfig(self, source, destination):
        config = self.loadNginxConfig()
        return

    def removeJsonConfig(self, name):

        # Remove JSON config for "name"
        return

    ##########################################3
    # Save NGINX Config block
    # Accepts JSON Config describing NGINX proxy blocks
    ##########################################3
    def saveNginxConfig(self, jsonConfig ):

        proxyList = jsonConfig["proxies"]

        # Print All Reverse-Proxy Blocks
        for entry in proxyList:

            print("Printing Block")
            print(entry["name"])
            block = '\n\n\n'
            block += self.printReverseProxyBlock(entry["source"], entry["destination"])
            print(block)
            self.writeNginxFile(block, entry["name"])
        return

    def printReverseProxyBlock(self, source, destination):

        print("Constructing ")
        block ='\n      location ' + source + ' {'
        block +='\n        proxy_pass ' + destination + '; '
        block +='\n        proxy_redirect off;'
        block +='\n        proxy_set_header Host $http_host;'
        block +='\n        proxy_set_header X-Real-IP $remote_addr;'
        block +='\n        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;'
        block +='\n        proxy_set_header X-Forwarded-Proto $scheme;'
        block +='\n        proxy_set_header X-Forwarded-Protocol $scheme;'
        block +='\n        proxy_set_header X-Url-Scheme $scheme;'
        block +='\n      }'
        block += '\n\n\n'

        return block

    def writeNginxFile(self, block, containerName):
        # path = '/users/sammy/days.txt'
        # days_file = open(path, 'r')
        # days = days_file.read()

        path = self.nginxConfigPath + containerName + '.conf'
        configFile = open(path, 'w')

        print("Writing Block")
        print(path)
        print(block)
        configFile.write(block)
        configFile.close()

    def removeNginxConfig(self, containerName):

        # Delete Config file from "nginxConfigPath + containerName + .conf"

        return



    ##########################################3
    # Sample Using NginxConfigBuilder
    ##########################################3
    # def generateConfig(self, json_obj):
    #
    #     nginx = NginxConfigBuilder(daemon='on')
    #     with nginx.add_server() as server:
    #
    #         server.add_route('/foo/',
    #                          proxy_pass='http://192.168.15.44/',
    #                          proxy_redirect="off",
    #                          include="/etc/nginx/proxy_params"
    #                          ).end()
    #
    #         server.add_route('/bar/',
    #                          proxy_pass='http://192.168.15.44/',
    #                          proxy_redirect="off",
    #                          include="/etc/nginx/proxy_params"
    #                          ).end()
    #
    #     print("Printing Server")
    #     print(server)
    #     print("Printing Configs")
    #     print(nginx)
    #
    #     return
    #


    # def loadFile(self, json_obj):
    #
    #     print("Loading Foo.conf")
    #     data = open("/etc/nginx/sites-available/foo.conf")
    #     print(data)
    #
    #     print("Parsing Foo.conf")
    #     nginxparser.load(data)
    #
    #     print("Dumping Foo.conf")
    #     result = nginxparser.dumps([['server'], [
    #         ['listen', '80'],
    #         ['server_name', 'foo.com'],
    #         ['root', '/home/ubuntu/sites/foo/']]])
    #
    #
    #     print(result)
    #     return
    #
    # def saveFile(self, json_obj):
    #     nginxparser.dumps([['server'], [
    #         ['listen', '80'],
    #         ['server_name', 'foo.com'],
    #         ['root', '/home/ubuntu/sites/foo/']]])
    #
    #
    #     return
