#!/usr/bin/python3

import json
import simplejson

import gitlab
from techworks.services.gitlab.GitLabService import GitLabService

import tornado.web
import tornado.escape
import tornado.httpclient

# ============================================
# Service which proxies calls to GitLab API
# ============================================
class ProjectService(GitLabService) :



    def options(self, *args, **kwargs):
        self.write({"success": False})

    def post(self):
        try:

            print("Executing Playbook")

            # Get json input
            data = tornado.escape.json_decode(self.request.body)

            # HostsFile
            hostsFile = data["hostsFile"]
            print ("Hosts File")
            print (hostsFile)
            self.write({"success": True})

        except:
            self.write({"success":False})


    def get(self):

        try:

            print("\n\n====================== Auth ======================")
            # username/password authentication (for GitLab << 10.2)
            # gl = gitlab.Gitlab('http://13.59.58.199/gitlab', email='root', password='d3v0lut10n')
            gl = gitlab.Gitlab('http://13.59.58.199/gitlab', private_token='NpsUxU6TamQB7GzgCYyu')

            # make an API request to create the gl.user object. This is mandatory if you use the username/password authentication.
            gl.auth()

            print("\n\n====================== Projects ======================")
            # Get list all the projects
            projects = gl.projects.list()

            # Get 1st Project
            project = gl.projects.get(11)

            for projectitem in projects:
                print(projectitem)

            print("\n\n====================== Attributes ======================")
            attributes = project.attributes
            print(project.attributes)
            print(json.dumps(project.attributes, indent=4, sort_keys=True))
            # for key, value in attributes.items():
            #     print("{} = {}".format(key, value))

            print("\n\n====================== Repository Tree ======================")
            repositories = []
            for file in project.repository_tree():
                print(json.dumps(file, indent=4, sort_keys=True))
                repositories.append(file)
            repositoriesdict = {"repositories":repositories}
            print(json.dumps(repositoriesdict, indent=4, sort_keys=True))

            print("\n\n====================== Users ======================")
            users = []
            for user in project.users.list():
                print(json.dumps(user.attributes, indent=4, sort_keys=True, skipkeys=True, ensure_ascii=True, check_circular=True))
                users.append(user.attributes)
            usersdict = {"users":users}
            print(json.dumps(usersdict, indent=4, sort_keys=True))

            print("\n\n====================== Members ======================")
            members = []
            for member in project.members.list():
                print(member)
                members.append(member.attributes)
            membersdict = {"members":members}
            print(json.dumps(membersdict, indent=4, sort_keys=True))

            # print("\n\n====================== Languages ======================")
            # languages = []
            # for language in project.languages():
            #     print(language)
            #     languages.append(language.attributes)
            # languagesdict = {"languages":languages}
            # print(json.dumps(languagesdict, indent=4, sort_keys=True))


            print("\n\n====================== Issues ======================")
            issues = []
            for issue in project.issues.list():
                print(json.dumps(issue.attributes, indent=4, sort_keys=True, skipkeys=True, ensure_ascii=True, check_circular=True))
                issues.append(issue.attributes)
            issuesdict = {"issues":issues}
            print(json.dumps(issuesdict, indent=4, sort_keys=True))


            print("\n\n====================== Response ====================== !!")
            success = {"success":True}
            response = dict(
                success=success,
                attributes=attributes,
                users=usersdict,
                members=membersdict,
                # languages=languagesdict,
                issues=issuesdict,
                repositories=repositoriesdict,
                test="test message"
            )
            print(json.dumps(response, indent=4, sort_keys=True))


            print("\n\n====================== Transmit ====================== !!")
            self.set_header('Content-Type', 'application/json')
            self.write(json.dumps(response, indent=4, sort_keys=True))
            # self.write({"success":True})
            self.flush()
            self.finish()


        except:
            self.write({"success":False})



