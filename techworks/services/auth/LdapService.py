#!/usr/bin/python3

import json
import simplejson

from techworks.services.BaseHandler import BaseHandler
import ldap

import tornado.web
import tornado.escape
import tornado.httpclient


# ============================================
# Service which proxies calls to GitLab API
# ============================================
class LdapService(BaseHandler):

    ldap_server = "10.14.126.102"
    ldap_url = 'ldap://10.14.126.102'

    username = "cn=admin,dc=devolution,dc=cloud"
    password = "d3v0lut10n"
    base_dn = "dc=devolution,dc=cloud"

    search_user="jason.hardman"
    search_filter = "uid=" + search_user

    def options(self, *args, **kwargs):
        self.write({"success": False})

    def get(self):

        try:
            print ("Initializing LDAP")
            connect = ldap.initialize(self.ldap_url)
            print (connect)
            # connect.set_option(ldap.OPT_REFERRALS, 0)

            print ("Binding Ldap")
            connect.simple_bind_s(self.username, self.password)

            print ("Searching Ldap")
            result = connect.search_s(self.base_dn, ldap.SCOPE_SUBTREE, self.search_filter)
            connect.unbind_s()
            print(result)

            self.write({"success": True})

        except ldap.LDAPError:
            connect.unbind_s()
            print("authentication error")
            self.write({"success":False})

    def post(self):

        try:
            self.write({"success": True})


        except:
            self.write({"success": False})

