#!/usr/bin/python3

from techworks.services.BaseHandler import BaseHandler
from techworks.drivers.VagrantDriver import VagrantDriver

###################################################
# Run Vagrant Command
###################################################
class VagrantCommandService(BaseHandler):

    def options(self, *args, **kwargs):
        self.write({"success": False})

    def get(self):
        try:
            print("Executing Vagrant Command")
            self.write({"success":True})

        except:
            self.write({"success":False})

    def post(self):
        try:

            print("Executing Vagrant Command")

            data = tornado.escape.json_decode(self.request.body)
            print("Data")
            print(data)
            # VagrantCommand
            operation = data["vagrantCommand"]
            print ("vagrantCommand : " + operation)

            machine_name = data["machine_name"]
            print ("machine_name : " + machine_name)

            snapshot_name = data["snapshot_name"]
            print ("snapshot_name : " + snapshot_name)


            # VagrantDriver : Get VMs & Playbooks
            vagrant = VagrantDriver()

            if(machine_name is not ""):
                msg = vagrant.executeVagrantCommand(operation, machine_name=machine_name, snapshot_name=snapshot_name)
            else:
                msg = vagrant.executeVagrantCommand(operation)


            print("msg")
            print(msg)

            json_msg = json.dumps({operation : msg})
            print("json_msg")
            print(json_msg)
            self.write(json_msg)
            # return json_msg

        except:
            self.write({"success":False})


