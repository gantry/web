import sockjs.tornado
from sockjs.tornado.router import SockJSRouter


class WebSocketConnection(sockjs.tornado.SockJSConnection):
    """Chat connection implementation"""

    # Set of WebSocket Connections
    # Represents "Chat Room Participants"
    participants = set()

    def sendMessage(self, message):
        self.broadcast(self.participants, message)

    def on_open(self, info):
        # Send that someone joined
        # Add client to the clients list
        print("WebSocket : Opened")
        self.broadcast(self.participants, "Someone joined.")
        self.participants.add(self)

    def on_message(self, message):
        # Broadcast message
        print("WebSocket : Received Message")
        text = message[::-1]
        # self.broadcast(self.participants, text)

    def on_close(self):
        print("WebSocket : Closed")
        # Remove client from the clients list and broadcast leave message
        self.participants.remove(self)
        self.broadcast(self.participants, "Someone left.")

    # def broadcast_message(self, message):
    #     print("WebSocket : Sending Message")
    #     # self.broadcast(self.participants, "this is a test")


