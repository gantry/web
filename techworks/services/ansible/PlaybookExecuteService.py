#!/usr/bin/python3

from techworks.services.BaseHandler import BaseHandler
from techworks.drivers.AnsibleDriver import AnsibleDriver


ansible = AnsibleDriver()


class PlaybookExecuteService(BaseHandler):

    def options(self, *args, **kwargs):
        self.write({"success": False})


    def get(self):
        try:
            print("Executing PlaybookHandler Command")
            self.write({"success":True})

        except:
            self.write({"success":False})


    def post(self):
        try:

            print("Executing Playbook")

            # # VagrantDriver : Get VMs & Playbooks
            # vagrant = VagrantDriver()
            # vm_names = vagrant.list_vms()
            # playbook_names = vagrant.list_playbooks()

            # Get json input
            data = tornado.escape.json_decode(self.request.body)

            # HostsFile
            hostsFile = data["hostsFile"]
            print ("Hosts File")
            print (hostsFile)

            # InstanceList
            instanceList = data["instanceList"]
            print ("Instance List")
            print (instanceList)

            # PlaybookPath
            playbookPath = data["playbookPath"]
            print ("Playbook File")
            print (playbookPath)


            for instance in instanceList :
                print("Instance")
                print (instance)

                innerInstance = instance["instance"]
                print("Inner Instance")
                print (innerInstance)

                instanceName = innerInstance["instanceName"]
                print("Instance Name")
                print (instanceName)

                playbooks = innerInstance["playbooks"]
                for playbook in playbooks :

                    # Add .yml extension to playbook name for ansible-playbook command
                    playbook = playbook.strip() + ".yml"

                    print("Playbook")
                    print(playbook)

                    # ===============================================
                    # VagrantDriver : Run Playbook
                    # vagrant.runPlaybook(vm_names, [playbook_names[1]])
                    # ===============================================

                    # ===============================================
                    # TODO : Find a way to pipe output back to screen
                    # The runPlaybook method needs upgrade for new API
                    # Find a way to pipe runPlaybookManual output to screen !!!
                    # ===============================================

                    # AnsibleDriver : Run Playbook
                    # results = ansible.runPlaybookAPI(hostsFile, instanceName, playbook_temp)

                    # runPlaybook(["test0", "test1"], ["install-common.yml"])
                    results = runPlaybookManual( instanceName, playbook)

                    # ===============================================

                    print("Results")
                    results = [instanceName, playbook]

                    # results = "[" + instanceName + ", " + playbook + "]"
                    print(results)

                    print("JSON Results")
                    json_msg = json.dumps({"results" : results})
                    print(json_msg)

                    # Write "partial reply" to browser
                    self.write(json_msg)
                    self.flush()

                    # self.write(json_msg)
                    # return json_msg
                    # return playbook_names

            # End the HTTP Request
            self.finish()

        except:
            self.write({"success":False})


    ###################################################
    # Run Ansible Playbooks across multiple servers
    ###################################################
    def runPlaybookManual(containers, playbooks):
        '''
        Run Ansible Playbooks
        '''
        ansible.runPlaybookManual(containers, playbooks)


